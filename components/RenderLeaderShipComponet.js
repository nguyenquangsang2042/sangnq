import React, { Component } from 'react';
import { View,FlatList } from 'react-native';
import { ListItem, Avatar } from 'react-native-elements';


class RenderLeaderShip extends Component {
    
  render() {
    return (
       
        <FlatList data={this.state.dishes}
        renderItem={({ item, index }) => this.renderMenuItem(item, index)}
        keyExtractor={item => item.id.toString()} />
       
     
      
    );
  }
  renderMenuItem(item, index) {
    const { navigate } = this.props.navigation;
    return (
      <ListItem key={index} onPress={() => navigate('Dishdetail', { dishId: item.id })}>
        <Avatar source={require('./images/uthappizza.png')} />
        <ListItem.Content>
          <ListItem.Title>{item.name}</ListItem.Title>
          <ListItem.Subtitle>{item.description}</ListItem.Subtitle>
        </ListItem.Content>
      </ListItem>
    );
  };
  onDishSelect(item) {
    //alert(item.id);
    this.setState({ selectedDish: item });
  }
}
export default RenderLeaderShip;